# Zabbix PoC

## Current setup

The main goal of current setup is presentation only.

```plantuml
@startuml
!pragma ratio 0.5
'skinparam padding 0
'skinparam nodesep 0
!theme crt-amber

title Zabbix Deployment on OpenShift

cloud Azure {
  package "Monitored Systems" as ams {
    node qa6xd-win11 as windows11 {
      component zabbix-agent as zaa {
      }
    }
  }
}

cloud OVH {
  package "Monitored Systems" as oms {
    [GitLab (Remote OVH DC)] as gitlab
  }

}

cloud Rogers {
  package "Monitored Systems" as rms {


 }


node "External Systems" {
  [PostgreSQL Database] as postgres
  node "Nginx Proxies" {
    [Nginx Proxy1 (HTTPS)] as nginx1
    [Nginx Proxy2 (HTTPS)] as nginx2
  }
  
  package "Monitored Systems" {

  }
}

cloud "OpenShift Cluster" as okd2 {
  node "Pod1" {
    [Zabbix-Server] as zabbix
    [Zabbix-Agent2 (sidecar)] as agent2
  }


  node "OpenShift Route" as route {
    [Port 8080 -> 8080]
  }
}


}

cloud Interner as internet {
node github
node dockerhub
}

' Relationships
[zabbix] --> postgres
[zabbix] --> [agent2]
[route] --> [zabbix]

nginx1 --> [Port 8080 -> 8080]
nginx2 --> [Port 8080 -> 8080]
[agent2] --> ams
[agent2] --> oms
[agent2] --> rms
[agent2] --> okd2
[agent2] --> github
[agent2] --> dockerhub
zaa --> agent2
@enduml
```

## Accessing Zabbix server

Tunnel to okd-c2 cluster is needed.

Example is at: https://gitlab.eclipse.org/eclipsefdn/it/releng/internal-services

Or:

```
ssh -T -F ~/.ssh/config -L 3443:internal-servicesc1.eclipse.org:443
```

..and part of `Caddyfile`:
```
zabbix-staging.eclipse.org {
	tls internal
	reverse_proxy https://localhost:3443 {
		transport http {
			tls_insecure_skip_verify
			tls_server_name "zabbix-staging.eclipse.org"
		}
	}
}
```

## Installation
Currently all the steps necessary to deploy zabbix are included in:
* .gitlab-ci.yml
* zabbix_values.yml
* gitlab CI/CD variables

## Adding checks
All checks of script type are added to scripts-configmap, based on content of scripts directory.

They are mounted in `zabbix-agent2` container at `/etc/zabbix/scripts` directory.

## Manual installation (deprecated)
helm repo add zabbix-community https://zabbix-community.github.io/helm-zabbix

Check available versions and set appropriate one:
```
helm search repo zabbix-community/zabbix -l
export ZABBIX_CHART_VERSION=4.2.1
```
Get available values:
```
helm show values zabbix-community/zabbix --version $ZABBIX_CHART_VERSION >  zabbix_values.yaml
```

Reinstall zabbix:
```
helm upgrade --install zabbix zabbix-community/zabbix  --dependency-update  --create-namespace  --version $ZABBIX_CHART_VERSION  -f ./zabbix_values.yaml -n zabbix --debug --dry-run
helm upgrade --install zabbix zabbix-community/zabbix  --dependency-update  --create-namespace  --version $ZABBIX_CHART_VERSION  -f ./zabbix_values.yaml -n zabbix 
```

