#!/bin/bash

# Set the path to the mounted secret
SECRET_FILE="/etc/zabbix/secrets/gpg-keys"
EXPIRY_THRESHOLD=7

GNUPGHOME=/tmp
export GNUPGHOME

# Initialize arrays
expired_keys_file=$(mktemp)
soon_to_expire_keys_file=$(mktemp)

# Function to check GPG key expiry
check_gpg_key() {
  local gpg_key_id=$1

  # Receive the key
  gpg --batch --keyserver keyserver.ubuntu.com --recv-keys $gpg_key_id &>/dev/null

  # List key details
  key_info=$(gpg --list-keys --with-colons $gpg_key_id 2>/dev/null)
  if [[ $? -ne 0 ]]; then
    echo "Error retrieving key information for $gpg_key_id" >&2
    return 1
  fi

  # Extract expiration date from uid line
  exp_date=$(echo "$key_info" | awk -F: '/^pub:/ {print $7}')
  if [[ -z $exp_date ]]; then
    echo "No expiration date found for key $gpg_key_id <br>" >&2
    return 1
  fi

  # Convert expiration date to seconds since epoch
  #exp_seconds=$(date -d "${exp_date}" +%s)
  exp_seconds=$exp_date
  now_seconds=$(date +%s)
  expiry_threshold_seconds=$(($now_seconds + $EXPIRY_THRESHOLD * 86400))

  if [[ $exp_seconds -lt $now_seconds ]]; then
    return 2  # Expired
  elif [[ $exp_seconds -lt $expiry_threshold_seconds ]]; then
    return 3  # Soon to expire
  fi

  return 0  # Valid
}

# Read the secret file, skipping the first empty line
tail -n +2 "$SECRET_FILE" | while IFS=: read -r org_name gpg_key_id; do
  # Trim leading and trailing whitespace
  org_name="${org_name#"${org_name%%[![:space:]]*}"}"
  org_name="${org_name%"${org_name##*[![:space:]]}"}"
  gpg_key_id="${gpg_key_id#"${gpg_key_id%%[![:space:]]*}"}"
  gpg_key_id="${gpg_key_id%"${gpg_key_id##*[![:space:]]}"}"

  check_gpg_key "$gpg_key_id"
  case $? in
    2)
      echo "$org_name:$gpg_key_id" >> "$expired_keys_file"
      ;;
    3)
      echo "$org_name:$gpg_key_id" >> "$soon_to_expire_keys_file"
      ;;
    *)
      ;;
    esac
done

# Read the results back from the temporary files
mapfile -t expired_keys < "$expired_keys_file"
mapfile -t soon_to_expire_keys < "$soon_to_expire_keys_file"

# Format the summary for Zabbix
output=""
if [ ${#expired_keys[@]} -ne 0 ]; then
  output+="Expired keys:\n"
  for key in "${expired_keys[@]}"; do
    output+="$key\n<br>"
  done
fi

if [ ${#soon_to_expire_keys[@]} -ne 0 ]; then
  output+="Soon to expire keys (within $EXPIRY_THRESHOLD days):\n"
  for key in "${soon_to_expire_keys[@]}"; do
    output+="$key\n<br>"
  done
fi

# Output the result for Zabbix
if [ -z "$output" ]; then
  echo "All keys are valid"
else
  echo -e "$output"
fi

