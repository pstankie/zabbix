import requests
import os

# Configuration
GITLAB_URL = "https://gitlab.eclipse.org"
PROJECT_ID = "23"  # Replace with your project ID

def get_gitlab_token():
    try:
        gitlab_token = os.environ['GITLAB_TOKEN']
        return gitlab_token
    except KeyError:
        raise Exception("GITLAB_TOKEN environment variable not set")

def get_open_issues_without_labels():
    url = f"{GITLAB_URL}/api/v4/projects/{PROJECT_ID}/issues"
    private_token = get_gitlab_token()
    headers = {
        "PRIVATE-TOKEN": private_token
    }
    params = {
        "state": "opened",
        "per_page": 100,
        "page": 1
    }
    open_issues_without_labels = []

    while True:
        response = requests.get(url, headers=headers, params=params)
        if response.status_code != 200:
            print(f"Error fetching issues: {response.status_code} - {response.text}")
            break

        issues = response.json()
        if not issues:
            break

        # Filter open issues without labels
        open_issues_without_labels.extend([issue for issue in issues if not issue.get("labels")])

        # Check if there are more pages
        if "next" in response.links:
            params["page"] += 1
        else:
            break

    return open_issues_without_labels

if __name__ == "__main__":
    issues = get_open_issues_without_labels()
    for issue in issues:
        issue_number = issue['iid']
        issue_title = issue['title']
        issue_link = f"{issue['web_url']}"
        print(f"Issue #{issue_number}: {issue_title} - {issue_link}<br>")

